package cz.itabsolvent.ita.domain;

 /*  @info   User class */
 /*  @author David Hegr, david.hegr.mail@gmail.com, 12/06/2020 */

import javax.persistence.*;
@Table(name = "user_account")   // přejmenování názvu databáze na "user_account" ; přes ALT+ENTER si nastavím datasource na tu databázi - zmizí červené podtržení
@Entity

public class User {         // v postgres je slovo User jedno z klíčových slov, stejně jako "select, set .." apod. proto musíme přejmenovat - viz anotace Table

    @Id                                                 // anotace z balíčku Java persistence - potřeba pro práci s User a s databází
    @GeneratedValue(strategy = GenerationType.AUTO)     // anotace z balíčku Java persistence - potřeba pro práci s User a s databází, automaticky generuje User ID
    public Long id;                                    // nastavuji typ "long", abych měl více prostoru na číslování uživatelů / úkol práce s databází
    public String firstName;        // označení řádku "a" na oranžovém podkladu značí, že spring počítá s tím, že toto bude sloupeček v databázi (dtto ostatní níže)

    @Column(name = "surname",length = 20)  // tato anotace říká, že u nás se "lastName" jmenuje tak, ale v databázi ho pojmenuj "surname". Řeší EN/US názvy příjmení. Default množství znaků je 255, na ukázku nastavuji 20
    public String lastName;
    public String email;
    public String phone;
    public String login;
/* nyní stisk levý ALT+INSERT otevře pop-up menu "Generate" a vyberu "Constructor".
Idea vygeneruje Constructor pro třídu User. */

    public User(){       // vložen bezparametrický konstruktor
    }

    /* toto níže Idea automaticky vygenerovala Constructor, po označení všech parametrů */
    public User(String firstName, String lastName, String email, String phone, String login) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.login = login;
    }

    public Long getId() {               // přes ALT+INSERT vložen getter and setter pro ID
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName(){           // přidáno dle přednášky s databází 03
        return firstName; }
    public void setFirstName(String firstName){
        this.firstName = firstName; }

    public String getLastName(){
        return lastName; }
    public void setLastName(String lastName){
        this.lastName = lastName; }

    public String getEmail(){
        return email; }
    public void setEmail(String email) {
        this.email = email;    }

    public String getPhone(){
        return phone; }
    public void setPhone(String phone) {
        this.phone = phone;    }

    public String getLogin(){
        return login; }
    public void setLogin(String login){
        this.login = login; }


}
