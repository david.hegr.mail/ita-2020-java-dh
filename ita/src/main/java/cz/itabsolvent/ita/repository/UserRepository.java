package cz.itabsolvent.ita.repository;


import cz.itabsolvent.ita.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository                             // anotace, zároveň abychom věděli, že to bude Beana
public interface UserRepository extends CrudRepository<User, Long> {        // CrudRepository automaticky připravil spring, znamená Create, Read, Update, Delete = CRUD. Jediné, co definujeme, jsou datové typy, nad kterými CRUD bude fungovat. Jedná se o generický repositář. První zadávám typ, který spravujeme (user) a potom typ Idčka (long).

    Optional<User> findByLogin(String Login);      // vloženo, příklad spring data - Optional<User> findById(Long aLong) ;; chci vyhledávat podle loginu ;; vyberu při psaní findByLogin z menu. Spring má načteného usera a na základě toho mi radí;
    List<User> findAllByLogin(String Login);       // stejně jako výše uvedené, jen vracím list uživatelů ; v místě "List<User> definuji, co má vracet - jestli List userů nebo usera atd.
                                                        // mohu dát i Optional, takže pokud ho nenajde, vrátí to optional empty a podle toho další chování


}
