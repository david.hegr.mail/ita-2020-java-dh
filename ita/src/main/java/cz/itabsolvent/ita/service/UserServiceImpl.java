package cz.itabsolvent.ita.service;

/*  @info   User Service */
/*  @author David Hegr, david.hegr.mail@gmail.com, 13/06/2020 */
// zde implementace UserService

import cz.itabsolvent.ita.model.UserResponseDto;
import cz.itabsolvent.ita.repository.UserRepository;
import cz.itabsolvent.ita.service.UserService;
import cz.itabsolvent.ita.domain.User;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceContext;
import java.util.List;

@Service    /* se založením tohoto souboru ve vrstvě Service, rovnou přidávám i aplikační logiku v podobě anotace Service - platí i pro ostatní soubory */


public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    public UserServiceImpl(UserRepository userRepository) {             // vytvořeno konstruktorem pod userRepository
        this.userRepository = userRepository;
    }

    public List<User> getAll() {
        return List.of(
                new User("Johny","Cash", "jc@idea.cz","608","JC"),
                new User("Pamela","Anderson","milaholka@idea.cz","609","PA"),
                new User("Ivanka","Hřoch","hřoch@idea.cz","606","HCH")
		);
    }

    @Override
    public User login(String login) {
        return new User(null,null, null,null,login);
    }

    public User getUser(String getUser) {
        return new User("Ivanka","Hřoch","hřoch@idea.cz","606","HCH");
    }

   // @Override
   // public User saveUser(User user) {           //vzniklo po stisku alt+enter nad "saveUser" a zavolání implementace v sobouru userservice. Tuto metodu voláme z UserControlleru
   //     return userRepository.save(user); // save Usera uloží, a uloženého ho vrátí zpátky. V Userovi při ukládání budeme generovat jeho ID, tzn. zadáváme ho bez ID, on ho při ukládání dogeneruje,  a vrátí ho zpět i s IDčkem
   // }   // postman nastavím metodu POST, nastavím v postam styl zprávy "Body", pod tím "Raw" a vpravo na řádku vyberu "JASON". Do okna níže vložím jasonovský zápis usera

    @Override
    public UserResponseDto saveUser(User user) {           //vzniklo po stisku alt+enter nad "saveUser" a zavolání implementace v sobouru userservice. Tuto metodu voláme z UserControlleru
        return mapToDto(userRepository.save(user)); // save Usera uloží, a uloženého ho vrátí zpátky. V Userovi při ukládání budeme generovat jeho ID, tzn. zadáváme ho bez ID, on ho při ukládání dogeneruje,  a vrátí ho zpět i s IDčkem
    }

    private UserResponseDto mapToDto(User entity) {
    return new UserResponseDto(
          entity.getFirstName(),
          entity.getLastName(),
          entity.getEmail(),
          entity.getPhone(),
          entity.getLogin()
    );
    }

    @Override
    public List<User> findAllByLogin(String login) {
        return userRepository.findAllByLogin(login);    //toto vzniklo implemetací z UserService, dopsali jsme za return místo "null", že má vrátit login.
    }


}