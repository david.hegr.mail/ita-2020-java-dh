package cz.itabsolvent.ita.service;
/*  @info   User Service */
/*  @author David Hegr, david.hegr.mail@gmail.com, 21/06/2020 */

import cz.itabsolvent.ita.domain.User;
import cz.itabsolvent.ita.model.UserResponseDto;

import java.util.List;

// zde všechny metody, public metody, které mají být vystaveny ven
public interface UserService {                      // tento soubor vytvořen přejmenováním původní USerService na UserServiceImpl, tato třída UserService je nová, vytvořena jako styl Interface..
    List<User> getAll();

    User login(String login);
   // User getUser(String getUser);

   // User saveUser(User user);   // metoda pro uložení entity do databáze. Ukážu na saveUser, dám alt+ENTER a implement method saveUser .. což vytvoří implementaci v souboru userserviceimpl.
   UserResponseDto saveUser(User user);

    List<User> findAllByLogin(String login);   //vyhledat všechny usery dle stejného loginu. Stisknu na závěr ALT+Enter a tím tuto metodu naimplementuji do UserServiceImpl



}