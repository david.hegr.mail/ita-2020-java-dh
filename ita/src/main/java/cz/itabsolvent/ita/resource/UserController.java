package cz.itabsolvent.ita.resource;

import cz.itabsolvent.ita.domain.User;
import cz.itabsolvent.ita.model.UserResponseDto;
import cz.itabsolvent.ita.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/*  @info   User Controller  -  vystavuje info o uživateli(-ích) */
/*  @author David Hegr, david.hegr.mail@gmail.com,12/06/2020 */

@RestController                 /* anotace Controller nebo RestController (JavaSpring). Spring třídu UserController díky
                                této anotaci nainstancuje do spring.boot aplikačního kontextu (viz soubor ItaApplication.java).
                                Idea pak mj. ví, že UserController bude Beana. */

public class UserController {
                            /* @Controller udělá z User Controller obecný controller, nejen pro RESTové API, např. pro webové služby.
                            Pro vystavování RESTových API je nutno upravit na @RestController.
                            Následně každá z definovaných metod v rámci class UserController bude RESTová.
                            */
    private UserService userService;    // toto je implementace UserService

    //----------------- PŘÍKLAD z přednášky: "aplikace data transfer object tříd Dto"
    /*  pro zabezpečení přenosů   */
  /* */
    @PostMapping("/login/{login}") //-- hejdova metoda na selekci uživatele dle loginu - u mně nenastaveno. zde jako ukázka
    public User login(@PathVariable("login") String login) {return userService.login(login);}

    @GetMapping("/user")
    public List<User> getUser() {return userService.getAll();}

    @PostMapping("/user")
    public UserResponseDto saveUser(@RequestBody User user) { return userService.saveUser(user);}

    @GetMapping("/user/login/{login}")
    public List<User> findAllByLogin(@PathVariable("login") String login) {return userService.findAllByLogin(login);}


    //----------------- PŘÍKLAD z přednášky: "vyhledání uživatelů podle loginu"
    /*  příklad z přednášky   */
  /*

    @GetMapping("/user/login/{login}")
    public List<User> findAllByLogin(@PathVariable("login") String login) {

        return userService.findAllByLogin(login);
    }
     */
    //----------------- PŘÍKLAD z přednášky: "Save entity, persintent layer, postman"
    /*  uložení entity do databáze    */
  /*
    //private UserService userService;    // toto je implementace UserService

    public UserController(UserService userService) {            //best practise - injectovat přes constructor a interface
        this.userService = userService;
    }
    @PostMapping("/user")
    public User saveUser(@RequestBody User user){
        return userService.saveUser(user);          // zavoláme servisu UserService, použijeme saveUser, a uložíme toho Usera.
    }*/
    // vstup, který přichází, je USER přes PostMana pomocí POST, je to @RequestBody -- proto v postmanu narveme do POST JASON zápis jako Usera. Přednáška 03, čas: 2:15:30
    // KOMENTÁŘ: důležité je v postmanovi "JASON" - v hlavičkách requestu mohou být různé údaje, ale typ kontentu v tomto případě musí být JASON.
    // Je to důležité proto, že ve springu máme "request body" anotaci, a tedy odpovídá POUZE na POSTy typu JASON (contenttype) .. jinak nereaguje vubec.

//----------------- PŘÍKLAD z přednášky: navajrování více userservices, než jen jednu (toto není dobrá praxe, ale lze to takto provést)
/*
    private List<UserService> userService;    // zde píšu, že chci naimplementovat více userService než jednu (použiju tedy List, stejně tak u metod pod ním)

    public UserController(List<UserService> userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> init() {
    return userService.get(0).getAll();     // číslem (např. nulou) v závorce určuji, která userservice dle indexu se má zde provést
    }
    @PostMapping
    public List<User> getUser() {
        return userService.get(0).getAll(); // číslem (např. nulou) v závorce určuji, která userservice dle indexu se má zde provést
    }
*/
//----------------- PŘÍKLAD z přednášky: Qualifier anotace - pojmenování
    /*
    private UserService userService;

    public UserController(@Qualifier("userservisa") UserService userService) {    // @Qualifier("userservisa") vyplním závorku pouze v případě, že bych měl více implementací
        this.userService = userService;                                            // následně v souboru "UserService" dám za anotaci Service závorku a vyplním do uvozovek totéž, tzn. "userservisa" a tím vznikne propojení.
    }
*/

//----------------- PŘÍKLAD z přednášky: PreDestroy anotace
    /* vyčištění zpráv, odpojení, uzavření databázové konexe - dá se ovlivnit co se má stát před tím, než je beana zničena */
/*
    @PreDestroy
        public void clean(){
        }
*/
//----------------- PŘÍKLAD z přednášky: PostConstruct anotace
    /* vše v rámci init pod PostConstruct se provede až v momentě, kdy je vše ostatní provedeno, nastaveno, dokončeno */
/*
    @PostConstruct
public void init(){
    }
*/
//----------------- PŘÍKLAD Homework I2-104: Postman - OK
    /* Podkladem příklad 4. Použiji anotaci PostMapping namísto GetMapping (jednoduše přepíšu get na post).*/
/*    private UserService userService;
    private User user;

    public UserController(UserService userService) {    // vygenerováno přes Constructor, pomocí menu Generování)
        this.userService = userService;                 //vygenerováno přes Constructor, pomocí menu Generování)
    }                                                   //vygenerováno přes Constructor, pomocí menu Generování)

    @GetMapping                                 // vrací trojici uživatelů - OK
    public List<User> test0() {
        return userService.getAll();
    }

    @PostMapping("/repeat/{toRepeat}")                                      // vrací uživatele s loginem
    public User repeat(@PathVariable("toRepeat") String toRepeat) {return new User("","","","",toRepeat);}

*/

//----------------- PŘÍKLAD 7: Postman: PostMapping s repeat - OK
                            /* Podkladem příklad 4. Použiji anotaci PostMapping namísto GetMapping (jednoduše přepíšu get na post).*/
/*
private UserService userService;

    public UserController(UserService userService) {    // vygenerováno přes Constructor, pomocí menu Generování)
        this.userService = userService;                 //vygenerováno přes Constructor, pomocí menu Generování)
    }                                                   //vygenerováno přes Constructor, pomocí menu Generování)

    @PostMapping("/repeat/{toRepeat}")                                     //path zadána ručně. Když nebudeme zadávat parametry metody,
    public String repeat(@PathVariable("toRepeat") String toRepeat) {      // tak on je schopen z té anotace path variable vytáhnout to co mu posíláme. Vloženo @PathVariable
        return toRepeat;                                                    // upraveno ručně.
    }
*/

                                /* KOMENTÁŘ K PŘÍKLADU: na url "/repeat/" my vložíme postmanem to, co chceme zopakovat. Jako vstupní parametr metody "repeat" jsme uvedli "toRepeat".
                                kterou tou metodou obhospodařujeme, dále že je to String, a že ten String zase pomocí return zase vracíme zpátky.
                                Krok2: Do postmana použiju POST a url localhost:8080/repeat/vepg   a server mi vrátí do postmana "vepg".
                                - Anotace PathVariable tedy slouží k vytahování dat o entitě na základě požadavku (výše ve formě "vepg") - např. roboti z přednášky.
                                - POST a PUT mohou mít tělo metody, mohou posílat nějaký payload. Informace neposílají jen v lomítku, umí odeslat Jason.
                                */

//----------------- PŘÍKLAD 6: Postman: PostMapping - OK
                            /* Podkladem příklad 4. Použiji anotaci PostMapping namísto GetMapping (jednoduše přepíšu get na post).
                               Následně při POST tak Postman vrací "Ahoj ze servisní vrstvy" */
/*
    private UserService userService;

    public UserController(UserService userService) {    // vygenerováno přes Constructor, pomocí menu Generování)
        this.userService = userService;                 //vygenerováno přes Constructor, pomocí menu Generování)
    }                                                   //vygenerováno přes Constructor, pomocí menu Generování)

    @PostMapping
    public String test3() {
        return userService.computeAhoj();
    }
*/


//----------------- PŘÍKLAD 5: Postman: GET - OK
                            /* V Postmanovi dám nové volání "GET" a budu provolávat localhost8080, bez další cesty - nyní se Postman chová stejně jako webový prohlížeč.
                            Pokud běží příklad 3 nebo 4, vrátí "Ahoj ze servisní vrstvy".
                            */

//----------------- PŘÍKLAD 4 - OK

                            /* PŘÍKLAD - Místo Autowired použijeme Constructor. Z příkladu 3 použiju GetMapping se všemi řádky a odeberu Autowired, ponechám pouze private UserService..
                               dám kurzor pod "private UserService userService, stisknu LEVÝ ALT + INSERT (nabídka Generování), vyberu Constructor, pak UserService, což vygeneruje UserController řádku)
                            */

    //private UserService userService;

    //public UserController(UserService userService) {    /* vygenerováno přes Constructor, pomocí menu Generování) */
    //    this.userService = userService;                 /* vygenerováno přes Constructor, pomocí menu Generování) */
    //}                                                   /* vygenerováno přes Constructor, pomocí menu Generování) */

    //@GetMapping
    //public String test3() {
    //    return userService.computeAhoj();
    //}
                            /* KOMENTÁŘ: použít Controller takto je výhodné např. v případě, kdy mám pouze jedinou UserService v rámci testu, neboť k ní mohu přistupovat pomocí controlleru přímo.) */
                            // Způsobem dle příkladu 4 a 3 jsme schopni do Bean injektovat jiné Beany a Spring zajišťuje celý background těchto operací.

//----------------- PŘÍKLAD 3 - OK

                            /* PŘÍKLAD - Nyní chceme, aby za "return" byl uveden výsledek nějaké logiky na servisní vrstvě a né obyčejný string s "Ahoj", jako je níže.*/
                            /* začnu vytvořením nové složky Service, s třídou (souborem) UserService. Po vytvoření tamní Beany se vracím zpět a začnu s Autovired anotací, která automaticky tu Beanu z UserService napojí sem. */
  //  @Autowired              /* tento příklad je ukázkou "dependency injection", tj. jednou z částí Inversion of Control. (IoC)*/
  //  private UserService userService;     /* zde říkám: Springu, zde chci mít implementaci Beany (třídy) UserService ze souboru UserService, tzn. někde ji vytvoř, a až bude někde vytvořená, tak mi ji sem dodej.*/

  //  @GetMapping
  //  public String test3() {
  //          return userService.computeAhoj();
  //      }

                            /* KOMENTÁŘ k příkladu: Výše zapsaný @RestController, má jendu závislost (jednu dependency) a je závislý na UserService. Tuto dependency sem Sprng sám doručil (z UserService) pomocí anotace Autowired. Anotace Autowired zabezpečí, aby se obsah injektoval na fieldu.
                               Idea podtrhla Autowired, neboť "field injection is not recommended". Je to proto, že my můžeme do těch tříd injektovat ostatní Beany pomocí Constructoru nebo pomocí Setteru.

//----------------- PŘÍKLAD 2 - NEFUNGUJE
                            /* Nefunkční, protože nemá converter. Veřejná metoda User, voláme metodu getUser, a ta vrací parametry ze souboru User v domain

    @GetMapping(path = "/seznam")
    public User getUser() {
        return new User("pepa", "zDepa","ímejl.cz","777900903", "loginx");
    } */

//----------------- PŘÍKLAD 1 - OK
                                /* anotace springu, mapuji se na metodu "GET", tzn. budeme provádět volání přes metodu "GET". Takto
                               se na portu 8080 (port Spring serveru) objeví localhost:8080 výpis "Ahoj". Když přidáme závorky a "path"
                               za GetMapping anotaci, bude se "Ahoj" promítat na dané adrese. */
                                    /* VÝPIS 1: Metoda, definující, kde budeme zobrazovat naše "resource", informace, apod. */
                                    /* výraz "String" vložen přes zkratku "ctrl+mezera". Metoda "test()" určující co se bude vypisovat na localhostu pomocí "return" */
 /*   @GetMapping
    public String test0() {
        return "Ahoj";
    }    */


}
